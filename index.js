class myArr {
  constructor(){
    if (arguments.length === 0) {
        this.length = 0
    } else if (arguments.length === 1 ) {
      if (typeof arguments[0] === "number"){
        if ( this.checkNumber(arguments[0]) ) {
          this.length = arguments[0]
        } else {
          throw new TypeError("Uncaught RangeError: Invalid array length");
        }
      } else {
        this[0] = arguments[0]
        this.length = 1
      }
    } else {
      for (let i = 0; i < arguments.length; i++) {
        this[i] = arguments[i]
      }
      this.length = arguments.length
    }
    Object.defineProperty(this, "_length", {
      enumerable: false,
      configurable: false,
      writable: true,
    })
  }
  checkNumber(value) {
    if (typeof(value) === "number" && !isNaN(value) && value >= 0 && value < Math.pow(2,32)-1 && value === ~~value ){
      return true
    }
    return false
  }
  set length (value) {
    if ( this.checkNumber(value) ) {
      while (this.length > value) {
        delete this[this.length - 1]
        this._length--
      } 
      this._length = value
    }
  }
  get length () {
    return this._length
  }
  [Symbol.iterator]() {
    let count = 0;
    let items = this;
    return {
      next() {
        if (count === items.length) {
          return { done: true}
        } else {
          return { value: items[count++], done: false }
        }
      }
    };
  } 
  push(){
    for (let i = 0; i < arguments.length; i++) {
      this[this.length] = arguments[i]
      this.length++
    }
    return this.length
  }
  pop(){
    let elem = this[this.length - 1]
    delete this[this.length - 1]
    this.length--
    return elem
  }
  shift(){ //Check
    let elem = this[0]
    delete this[0]
    for (let i = 0; i < this.length; i++) {
      //this[i + 1] ? this[i] = this[i + 1] : this[i] = undefined
      this[i] = this[i + 1]
      delete this[i + 1]
      this.length--
    }
    return this.length === 0 ? undefined : elem
  }
  unshift(){ //Check
    for ( let i = this.length + arguments.length; i > arguments.length; i--) {
      this[i - arguments.length-1] ? this[i] = this[i - arguments.length-1] : void 0
      delete this[i - arguments.length]
    }
    for (let i = 0; i < arguments.length; i++) {
      this[i] = arguments[i]
      this.length++
    }
    return this.length
  }
  map (callback, thisArg) {
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    const result = new myArr;
    for (let i = 0; i < this.length; i++) {
      this[i] ? result[i] = callback.call(thisArg, this[i], i, this) : void 0
      result.length++
    }
    return result;
  };
  forEach(callback, thisArg) {
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    for (let i = 0; i < this.length; i++) {
      this[i] ? callback.call(thisArg, this[i], i, this) : void 0 
      
    }
  }
  reduce(callback, initialValue) {
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    let accumulator = 0;
    initialValue ? accumulator = initialValue : void 0
    for (let i = 0; i < this.length; i++) {
      this[i] ? accumulator = callback(accumulator, this[i], i, this) : void 0
    }
    return accumulator
  };
  static from(){
    let items, mapFn, iValue, thisArg;
    for(let i = arguments.length; i > arguments.length - 2; i--) {
      if (typeof arguments[i] === 'function' ) {
        mapFn = arguments[i]
        delete arguments[i]
        arguments.length = i
        if (typeof arguments[i + 1] === "object") {
          thisArg = arguments[i + 1];
          delete arguments[i + 1]
        }
      }
    }
    arguments.length === 1 ? items = arguments[0] : items = arguments;
    if (items == null) {
      throw new TypeError('Array.from requires an array-like object - not null or undefined');
    }
    let result = new myArr()
    result.length = 0
    for( let i = 0; i < items.length; i++) {
      iValue = items[i];
      if (mapFn) {
        result[i] = typeof thisArg === 'undefined' ? mapFn(iValue, i) : mapFn.call(thisArg, iValue, i);
      } else {
        result[i] = iValue;
      }
      result.length++
    }
    return result;
  }
  toString(){
    let result = "", item;
    this.length === 1 ? item = this[0] : item = this
    for (let i = 0; i < item.length ; i++) {
      item[i] ? result += item[i] + "," : result += ','
    }
    return result.slice(0, result.length-1)
  }
  sort(){
    let temp, items, compareFunction;
    typeof arguments[0] === "function" ? compareFunction = arguments[0] : void 0
    this.length === 1 ? items = this[0] : items = this
    function compareBubble(a, b) {
      a = a + ""
      b = b + ""
      if (a > b) return 1;
      if (a == b) return 0;
      if (a < b) return -1;
    }
    const compare = compareFunction || compareBubble
    let counter = items.length;
    while (counter){
      for(let i = 1; i < counter; i++) {
        if (compare(items[i - 1], items[i]) > 0) { 
          temp = items[i],
          items[i] = items[i - 1]
          items[i - 1] = temp
          if (!items[i] && typeof items[i-1] !== "undefined") {
            delete items[i]
          }
        }
      } 
      counter--  
    }
    return this
  }
  filter(callback, thisArg){
    let items, temp;
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    this.length === 1 ? items = this[0] : items = this
    const result = new myArr;
    for (let i = 0; i < items.length; i++) {
      temp = callback.call(thisArg, items[i], i, items);
      if (temp){
        result.length++
        result[result.length] = items[i]
      }
    }
    return result;
  }
}
